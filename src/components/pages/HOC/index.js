import React from "react";
import SearchTodos from "./TodoList";
import SearchUsers from "./userList";
import "./first.css";
const index = () => {
  return (
    <div className="App">
      <h2 className="title">Higher Order Component</h2>
      <div className="section">
        <div className="mainteg">
          <SearchUsers />
        </div>
        <div className="mainteg">
          <SearchTodos />
        </div>
      </div>
    </div>
  );
};

export default index;
