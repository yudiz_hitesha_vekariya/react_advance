import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import index from "./components/pages/HOC/index";
import Navbar from "./components/navbar/Navbar";
import Accessibility from "./components/pages/Accessibility/Accessibility";

function App() {
  return (
    <>
      <Router>
        <Navbar />
        <div className="pages">
          <Switch>
            <Route exact path="/" component={Accessibility} />
            <Route exact path="/hoc" component={index} />
          </Switch>
        </div>
      </Router>
    </>
  );
}

export default App;
