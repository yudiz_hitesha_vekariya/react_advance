import React from "react";

export const themes = {
  dark: {
    color: "rgb(197, 197, 26)",
    background: "rgb(104, 104, 133)",
  },
  light: {
    color: "rgb(5, 5, 58)",
    background: "rgb(197, 197, 26)",
  },
};

const ThemeContext = React.createContext(themes.dark);

export default ThemeContext;
