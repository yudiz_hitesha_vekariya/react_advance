import React, { useState } from "react";
import ThemeContext, { themes } from "./theme-context";
import { useHistory } from "react-router-dom";
import "./Accessibility.css";

const Accessibility = () => {
  const [theme, setTheme] = useState(themes.dark);

  const toggleTheme = () =>
    theme === themes.dark ? setTheme(themes.light) : setTheme(themes.dark);
  const history = useHistory();
  const clickFunc = () => {
    alert("submitted successfull!!", history.push("./hoc"));
  };

  return (
    <>
      <ThemeContext.Provider value={theme}>
        <button className="btn" onClick={toggleTheme}>
          CHANGE THEME
        </button>

        <div className="box" style={theme}>
          <div className="main">
            <h1 >LOGIN PAGE</h1>
            <br></br>
            <label htmlFor="email"> Email ID</label>
            <br></br>
            <br></br>
            <input
              id="email"
              type="text"
              placholder="Enter Your email..."
              aria-label="Email"
              aria-required="true"
            />
            <br></br>
            <br></br>
            <label htmlFor="pwd"> Password</label>
            <br></br>
            <br></br>
            <input
              id="pwd"
              type="text"
              placholder="Enter Your password..."
              aria-label="Password"
              aria-required="true"
            />
            <br></br>
            <br></br>
            <button onClick={clickFunc}>Submit</button>
          </div>
        </div>
      </ThemeContext.Provider>
    </>
  );
};

export default Accessibility;
