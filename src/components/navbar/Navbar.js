import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import "./NavBar.css";

function Navbar() {
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);
  return (
    <>
      <nav className="navbar">
        <div className="nav-container">
          <NavLink exact to="/" className="nav-logo">
            <i class="fa-brands fa-react"></i>
            _ADVANCE
          </NavLink>

          <ul className={click ? "nav-menu active" : "nav-menu"}>
            <li className="nav-item">
              <NavLink
                exact
                to="/"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                <i class="fa-solid fa-house-user"></i>
                Accessibility
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/hoc"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                <i class="fa-solid fa-house-user"></i>
                HOC
              </NavLink>
            </li>
          </ul>
          <div className="nav-icon" onClick={handleClick}>
            <i className={click ? "fas fa-times" : "fas fa-bars"}></i>
          </div>
        </div>
      </nav>
    </>
  );
}

export default Navbar;
